package ttech.sopler;

/*
 Created by Tryfon Tz.
 Copyright (c) 2014 Tryfon Tz. All rights reserved.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

//import com.google.analytics.tracking.android.EasyTracker;

public class MainActivity extends Activity {

    WebView mWebView;
    String sopler_url = "https://sopler.net/home";

    private static final long delay = 2000L;
    private boolean mRecentlyBackPressed = false;
    private Handler mExitHandler = new Handler();
    private final Runnable mExitRunnable = new Runnable() {

        @Override
        public void run() {
            mRecentlyBackPressed = false;
        }
    };

    ProgressBar loadingProgressBar, loadingTitle;

    public static GoogleAnalytics analytics;
    public static Tracker tracker;
    private Tracker mTracker;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);

        analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(1800);

        tracker = analytics.newTracker("UA-46431000-5"); // Replace with actual tracker id
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);
/*

        // [START shared_tracker]
        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        // [END shared_tracker]
*/

        mWebView = (WebView) findViewById(R.id.webView1);
        // Enable JS
        mWebView.getSettings().setJavaScriptEnabled(true);
        // Multi-windows
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        // mWebView.getSettings().setSupportMultipleWindows(true);
        //mWebView.setWebViewClient(new MyWebViewClient());

        //loadingProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sopler_url));
        startActivity(browserIntent);

        Toast.makeText(this, R.string.sopler_started,Toast.LENGTH_SHORT).show();

        final Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sopler_url));
                startActivity(browserIntent);
            }
        });

       /*
        mWebView.setWebChromeClient(new WebChromeClient() {
            // this will be called on page loading progress
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                loadingProgressBar.setProgress(newProgress);
                // hide the progress bar if the loading is complete
                if (newProgress == 100) {
                    loadingProgressBar.setVisibility(View.GONE);
                } else {
                    loadingProgressBar.setVisibility(View.VISIBLE);
                }
            }
        });
        mWebView.loadUrl(sopler_url);
        */
    }

/*

    @Override
    public void onStart() {
        super.onStart();
        //EasyTracker.getInstance(this).activityStart(this);
        // mWebView.reload();
    }

    @Override
    public void onStop() {
        super.onStop();
        //EasyTracker.getInstance(this).activityStop(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("Resume", "Setting screen name: " + "Resume");
        mTracker.setScreenName("Image~" + "Resume");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
*/


    /*
    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // In the WebView
            // view.loadUrl(url);
            // return true;

            // If not sopler.net open browser
            if (Uri.parse(url).getHost().contains("sopler.net")) {
                return false;
            }
            return true;
        }

        // Custom Webpage error page
        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            mWebView.loadUrl("file:///android_asset/errorpage.html");
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // hide loading image
            findViewById(R.id.imageView1).setVisibility(View.GONE);
            // show webview
            findViewById(R.id.webView1).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            mWebView.goBack();
            // mWebView.reload();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up
        // to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }
    */

    @Override
    public void onBackPressed() {

        // 2 Back finishes the app
        if (mRecentlyBackPressed) {
            mExitHandler.removeCallbacks(mExitRunnable);
            mExitHandler = null;
            super.onBackPressed();
        } else {
            mRecentlyBackPressed = true;
            Toast.makeText(this, R.string.back_twice_message,
                    Toast.LENGTH_SHORT).show();
            mExitHandler.postDelayed(mExitRunnable, delay);
        }
    }
}